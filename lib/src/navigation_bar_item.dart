import 'package:flutter/material.dart';

class TitledNavigationBarItem {
  final Widget title;
  Widget icon;
  final Color backgroundColor;
  final bool showNotification;
  final int notificationCount;

  TitledNavigationBarItem({
    @required this.icon,
    @required this.title,
    @required this.showNotification,
    this.backgroundColor = Colors.white,
    this.notificationCount = 0
  }){
    if(showNotification)
      this.icon = _iconWithNotification();
  }

  Widget _iconWithNotification(){
    return  Container(
        width: 24,
        height: 24,
        child: Stack(
          children: [
            this.icon,
            Container(
              width: 24,
              height: 24,
              alignment: Alignment.topRight,
              margin: EdgeInsets.only(bottom:0),
              child: Container(
                width: this.notificationCount == 0 ? 11 : 14,
                height: this.notificationCount == 0 ? 11 : 14,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.red[600],
                    border: Border.all(
                        color: Colors.white, width: 1)),
                child: Padding(
                  padding: const EdgeInsets.all(0),
                  child: Center(
                    child: Text(
                      this.notificationCount == 0 ? '' : (notificationCount.toString().length > 1 ? '9+' : notificationCount.toString()),
                      style: TextStyle(
                          fontSize: notificationCount.toString().length > 1 ? 8 : 10, color: Colors.white),
                    ),
                  ),
                ),
              ),
            )

          ],
        ));
  }

}
