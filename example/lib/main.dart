import 'package:flutter/material.dart';
import 'package:titled_navigation_bar/titled_navigation_bar.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Titled Bar',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  State createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool navBarMode = true,showNoti = false;

  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration(seconds: 5)).then((value) => setState((){showNoti = true;}));
    return Scaffold(
      appBar: AppBar(
        title: Text("Titled Bottom Bar"),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text("Reversed mode:"),
            Switch(
              value: navBarMode,
              onChanged: (v) {
                setState(() => navBarMode = v);
              },
            ),
          ],
        ),
      ),
      bottomNavigationBar: TitledBottomNavigationBar(
        onTap: (index) => print("Selected Index: $index"),
        reverse: navBarMode,
        curve: Curves.easeInBack,
        items: [
          TitledNavigationBarItem(title: Text('Home'), icon: Icon(Icons.home,color: navBarMode ? Colors.blueGrey : Colors.red,),showNotification: true),
          TitledNavigationBarItem(title: Text('Search'), icon: Icon(Icons.search,color: navBarMode ? Colors.blueGrey : Colors.red,),showNotification: true),
          TitledNavigationBarItem(title: Text('Bag'), icon: Icon(Icons.card_travel,color: navBarMode ? Colors.blueGrey : Colors.red,),showNotification: showNoti),
          TitledNavigationBarItem(title: Text('Orders'), icon: Icon(Icons.shopping_cart,color: navBarMode ? Colors.blueGrey : Colors.red,),showNotification: true,notificationCount: 19),
          TitledNavigationBarItem(title: Text('Profile'), icon: Icon(Icons.person_outline,color: navBarMode ? Colors.blueGrey : Colors.red,),showNotification: true),
        ],
        activeColor: Colors.red,
        inactiveColor: Colors.blueGrey,
      ),
    );
  }
}
